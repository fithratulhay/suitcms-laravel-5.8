<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pages', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('parent_id')->unsigned()->nullable();
            $table->string('url_prefix')->default('');
            $table->string('slug')->nullable()->index();
            $table->string('layout')->nullable();

            $table->dateTime('published_at')->nullable();
            $table->timestamps();

            $table->foreign('parent_id')
                  ->references('id')->on('pages')
                  ->onDelete('cascade');
        });


        \Schema::create('pages_translate', function (Blueprint $table) {
            $table->integer('page_id')->unsigned();
            $table->string('lang', 10)->index();
            $table->string('title');
            $table->text('description')->nullable();
            $table->mediumText('content')->nullable();

            $table->foreign('page_id')
                  ->references('id')->on('pages')
                  ->onDelete('cascade');

            $table->primary(['page_id', 'lang']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pages_translate');
        Schema::dropIfExists('pages');
    }
}
