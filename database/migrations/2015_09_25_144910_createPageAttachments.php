<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePageAttachments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \Schema::create('page_attachments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('page_id')->unsigned();
            $table->string('name', 20)->index();
            $table->string('uri', 500);
            $table->tinyInteger('uri_type')->default(0);
            $table->text('uri_info')->default('');

            $table->timestamps();

            $table->foreign('page_id')
                 ->references('id')->on('pages')
                 ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \Schema::drop('page_attachments');
    }
}
