<?php

namespace App\Supports\Attachment;

use Exception;

class FormatNotSupportException extends Exception
{
}
