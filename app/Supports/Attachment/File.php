<?php

namespace App\Supports\Attachment;

class File
{

    protected $urlPath;

    protected $mimeType;

    protected $size;

    public function __construct($urlPath, $mimeType, $size)
    {
        $this->urlPath = $urlPath;

        $this->mimeType = $mimeType;

        $this->size = $size;
    }

    public function getUrlPath()
    {
        return $this->urlPath;
    }

    public function getMimeType()
    {
        return $this->mimeType;
    }

    public function getSize()
    {
        return $this->size;
    }

    public function get($field)
    {
        $method = 'get' . ucfirst($field);
        if (method_exists($this, $method)) {
            return call_user_func([$this, $method]);
        }
        return null;
    }

    public function toArray()
    {
        return [
            'url_path' => $this->getUrlPath(),
            'mime_type' => $this->getMimeType(),
            'size' => $this->getSize()
        ];
    }
}
