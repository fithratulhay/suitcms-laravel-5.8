<?php

namespace App\Model;

use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;
use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\Access\Authorizable;

class User extends BaseModel implements
    AuthenticatableContract,
    AuthorizableContract,
    CanResetPasswordContract,
    SluggableInterface
{
    use Authenticatable;
    use Authorizable;
    use CanResetPassword;
    use SluggableTrait;

    /**
     * User Group
     */
    const USER = 0;
    const SUPER_ADMIN = 1;

    protected $table = 'users';

    public $timestamps = true;

    protected $sluggable = [
        'build_from' => 'username'
    ];

    protected $urlKey = 'slug';

    protected $fillable = [
        'group_type',
        'username',
        'name',
        'email',
        'password',
        'slug',
        'about',
        'date_of_birth',
        'active',
    ];

    protected $hidden = [
        'password', 'remember_token'
    ];

    protected $searchField = [
        'username',
        'name',
    ];

    public function setPasswordAttribute($value)
    {
        if (!empty($value) || is_numeric($value)) {
            $this->attributes['password'] = \Hash::make($value);
        }
    }

    public static function groups()
    {
        return [
            static::SUPER_ADMIN => 'Super Administrator',
            static::USER => 'User'
        ];
    }

    public function getGroupTypeAttribute()
    {
        if (!isset($this->attributes['group_type'])) {
            return null;
        }

        $group = $this->attributes['group_type'];
        if (!is_numeric($group)) {
            return -1;
        }
        return (int)$group;
    }

    public function isSuper()
    {
        return $this->group_type === static::SUPER_ADMIN;
    }

    public function isUser()
    {
        return $this->group_type === static::USER;
    }

    public function login()
    {
        if ($this->current_log_in_at !== null) {
            $this->last_log_in_at = $this->current_log_in_at;
            $this->last_log_in_ip = $this->current_log_in_ip;
        }

        $this->current_log_in_at = new \DateTime;
        $this->current_log_in_ip = \Request::getClientIp();
        $this->save();

        $this->increment('log_in_count');
    }

    public function logout()
    {
        $this->last_log_in_at = new \DateTime;
        $this->last_log_in_ip = \Request::getClientIp();
        $this->current_log_in_at = null;
        $this->current_log_in_ip = '';
        $this->save();
    }

    public function getName()
    {
        return $this->name;
    }

    public function getGroupName()
    {
        if (!in_array($this->group_type, array_keys(static::groups()))) {
            throw new \Exception('Group type [' . $this->group_type . '] is not defined');
        }

        return static::groups()[$this->group_type];
    }

    public function scopeNotMe($query)
    {
        if (\Auth::check()) {
            return $query->where('id', '<>', \Auth::user()->id);
        }
    }

    public function isMe()
    {
        $auth = \Auth::user();
        return get_class($this) == get_class($auth) && $this->id === $auth->id;
    }

    public function listById()
    {
        $users = $this->get();
        $res = [];
        foreach ($users as $user) {
            $res[$user->getKey()] = $user->name . ' (' . $user->getGroupName() . ')';
        }
        return $res;
    }
}
