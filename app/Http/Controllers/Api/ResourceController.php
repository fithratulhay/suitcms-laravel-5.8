<?php
namespace App\Http\Controllers\Api;

use App\Model\BaseModel;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use IteratorAggregate;

abstract class ResourceController extends Controller
{
    const DEFAULT_PER_PAGE = 10;

    const MAX_PER_PAGE = 1000;

    protected $model;

    protected $filterFields = [];

    public function __construct(BaseModel $model)
    {
        parent::__construct();

        $this->model = $model;
    }

    public function index(Request $request)
    {
        $perPage = $this->getRequestPerPage($request);

        $query = $this->createQueryFromRequest($request);
        $paginator = $query->paginate($perPage);
        $paginator->appends($request->query());

        $visibleFields = $this->getVisibleFields($request);
        $hiddenFields = $this->getHiddenFields($request);
        $this->filterVisible($paginator, $visibleFields, $hiddenFields);
        return $paginator;
    }

    public function show(Request $request, $key)
    {
        $query = $this->createQueryFromRequest($request);

        $model = $query->findOrFail($key);
        $visibleFields = $this->getVisibleFields($request);
        $hiddenFields = $this->getHiddenFields($request);
        $this->filterVisible($model, $visibleFields, $hiddenFields);
        return $model;
    }

    protected function getRequestPerPage(Request $request)
    {
        $perPage = $request->get('per_page', static::DEFAULT_PER_PAGE);
        if ($perPage >= static::MAX_PER_PAGE) {
            return static::MAX_PER_PAGE;
        }
        return $perPage;
    }

    protected function createQueryFromRequest(Request $request)
    {
        $params = $request->query();

        $filterParams = array_filter($params, function ($key) {
            return starts_with($key, 'filter_');
        }, ARRAY_FILTER_USE_KEY);

        foreach ($filterParams as $key => $value) {
            sscanf($key, "filter_%s", $column);
            $filterParams[$column] = $value;
            unset($filterParams[$key]);
        }

        $orderParams = array_filter($params, function ($key) {
            return starts_with($key, 'order_');
        }, ARRAY_FILTER_USE_KEY);

        foreach ($orderParams as $key => $value) {
            sscanf($key, "order_%s", $column);
            $column = str_replace(':', '.', $column);
            $orderParams[$column] = $value;
            unset($orderParams[$key]);
        }
        $searchQuery = $request->get('s');

        return $this->model->filter($searchQuery, $orderParams, $filterParams)->newQuery();
    }

    protected function filterVisible($models, $visibleFields = null, $hiddenFields = null)
    {
        if ($models instanceof Model) {
            $this->setModelVisiblity($models, $visibleFields, $hiddenFields);
            return;
        }

        if ($models instanceof IteratorAggregate ||
            is_array($models)
        ) {
            foreach ($models as $model) {
                $this->filterVisible($model, $visibleFields, $hiddenFields);
            }
        }
    }

    protected function setModelVisiblity($models, $visibleFields, $hiddenFields)
    {
        if (empty($visibleFields)) {
            $visibleFields = [];
        }
        if (empty($hiddenFields)) {
            $hiddenFields = [];
        }
        $relVisibleFields = array_filter($visibleFields, function ($value) {
            return str_contains($value, '.');
        });
        $visibleFields = array_filter($visibleFields, function ($value) {
            return !str_contains($value, '.');
        });

        $relHiddenFields = array_filter($hiddenFields, function ($value) {
            return str_contains($value, '.');
        });
        $hiddenFields = array_filter($hiddenFields, function ($value) {
            return !str_contains($value, '.');
        });

        $relVisibleFields = $this->flatArrayToNestedArray($relVisibleFields);
        $relHiddenFields = $this->flatArrayToNestedArray($relHiddenFields);

        foreach ($models->getRelations() as $relationName => $relationInstance) {
            $snakeRelationName = snake_case($relationName);
            $currentHiddenFields = array_get($relHiddenFields, $snakeRelationName);
            $currentVisibleFields = array_get($relVisibleFields, $snakeRelationName);
            $this->filterVisible($relationInstance, $currentVisibleFields, $currentHiddenFields);

            if (in_array($snakeRelationName, $visibleFields)) {
                $visibleFields[] = $relationName;
                unset($visibleFields[$snakeRelationName]);
            }
            if (in_array($snakeRelationName, $hiddenFields)) {
                $hiddenFields[] = $relationName;
                unset($hiddenFields[$snakeRelationName]);
            }
        }

        $models->setVisible($visibleFields);
        $models->setHidden(array_merge($models->getHidden(), $hiddenFields));
        $models->setDateFormat(Carbon::ISO8601);
    }

    public static function flatArrayToNestedArray($flatArray)
    {
        $result = [];
        foreach ($flatArray as $value) {
            $segments = explode('.', $value);
            $key = array_shift($segments);
            if (!isset($result[$key])) {
                $result[$key] = [];
            }
            $result[$key][] = implode('.', $segments);
        }
        return $result;
    }

    protected function getVisibleFields(Request $request)
    {
        return $this->parseFields($request->get('fields'));
    }

    protected function getHiddenFields(Request $request)
    {
        return $this->parseFields($request->get('hidden_fields'));
    }

    protected function parseFields($rawFields)
    {
        if (empty($rawFields)) {
            return null;
        }
        $fields = explode(',', $rawFields);
        $fields = array_map('trim', $fields);
        return array_unique($fields);
    }
}
