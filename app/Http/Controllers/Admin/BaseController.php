<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

abstract class BaseController extends Controller
{
    /**
     * Custom view prefix
     * @var string
     */
    protected $viewPrefix;

    /**
     * Custom route prefix
     * @var string
     */
    protected $routePrefix;

    /**
     * Custom page name
     * @var string
     */
    protected $pageName;

    public function __construct()
    {
        parent::__construct();

        view()->share([
            'routePrefix' => $this->getRoutePrefix(),
            'viewPrefix' => $this->getViewPrefix(),
            'pageName' => $this->getPageName(),
            'nav' => $this->getControllerName(),
        ]);
    }

    /**
     * Get Controller name without 'Controller' postfix
     * @return string
     */
    protected function getControllerName()
    {
        return preg_replace("/(.*)[\\\\](.*)(Controller)/", '$2', get_class($this));
    }

    /**
     * Get View Prefix. By default the value is plurar from and snake case of controller name
     * @return string
     */
    protected function getViewPrefix()
    {
        if ($this->viewPrefix !== null) {
            return $this->viewPrefix;
        }

        return str_plural(snake_case($this->getControllerName()));
    }

    /**
     * Get Route Prefix. By default the value is plurar from and snake case of controller name
     * @return string
     */
    protected function getRoutePrefix()
    {
        if ($this->routePrefix !== null) {
            return $this->routePrefix;
        }

        return str_plural(snake_case($this->getControllerName(), '-'));
    }

    /**
     * Get Page header for page title.  By default the value is uppercase word and snake case of controller name
     * @return [type] [description]
     */
    protected function getPageName()
    {
        if ($this->pageName !== null) {
            return $this->pageName;
        }

        $name = snake_case($this->getControllerName(), '-');
        $name = implode(' ', explode('-', $name));
        return ucwords($name);
    }
}
