@extends('admins._layouts.index-base')

@section('page-title')
    {{ $pageName }}
@endsection

@section('page-breadcrumb')
    @parent
    <li>
        <i class="fa fa-angle-right"></i> {{ $pageName }}
    </li>
@endsection

@section('table-title')
    {{ $pageName }} Table
@endsection

@section('page-header-toolbar')
    <div class="btn-group pull-right">
        <a href="{{ suitRoute($routePrefix.'.create') }}" class="btn btn-sm btn-primary"><i class="glyphicon glyphicon-plus"></i>&nbsp;Add New</a>
    </div>
@endsection

@section('table-column-checkbox')
    <label><input type="checkbox" data-name="id">#</label>
    <label><input type="checkbox" checked data-name="title">Title</label>
    <label><input type="checkbox" checked data-name="parent.title">Parent</label>
    <label><input type="checkbox" checked data-name="description">Description</label>
    <label><input type="checkbox" checked data-name="published_at">Published</label>
    <label><input type="checkbox" checked data-name="created_at">Created At</label>
    <label><input type="checkbox" data-name="updated_at">Updated At</label>
@endsection

@section('table-filter')
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                {!! Form::select('title', [], '', ['class' => 'form-control form-filter select2me', 'placeholder' => 'Select Title']) !!}
            </div>
        </div>
    </div>
    <div class="form-actions">
        <div class="row">
            <div class="col-md-12">
                {!! Form::button('Submit', ['class' => 'btn blue form-filter-submit']) !!}
                {!! Form::button('Reset', ['class' => 'btn yellow form-filter-reset']) !!}
            </div>
        </div>
    </div>
    <hr>
@stop

