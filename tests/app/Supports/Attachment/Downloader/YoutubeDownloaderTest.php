<?php

namespace TestApp\Supports\Attachment\Downloader;

use App\Supports\Attachment\Downloader\YoutubeDownloader;
use App\Supports\Attachment\Youtube;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\BadResponseException;
use GuzzleHttp\Message\Response;
use GuzzleHttp\RedirectMiddleware;
use Illuminate\Contracts\Filesystem\Filesystem;
use Mockery;
use Symfony\Component\HttpFoundation\File\MimeType\MimeTypeExtensionGuesser;
use TestApp\TestCase;

class YoutubeDownloaderTest extends TestCase
{
    protected $filesystem;

    protected $client;

    protected $downloader;

    public function setUp()
    {
        parent::setUp();

        $filesystem = Mockery::mock(Filesystem::class);
        $client = Mockery::mock(Client::class);
        $extensionGuesser = new MimeTypeExtensionGuesser;

        $downloader = Mockery::mock(YoutubeDownloader::class, [$filesystem, $extensionGuesser, $client])->makePartial();

        $this->filesystem = $filesystem;
        $this->client = $client;
        $this->downloader = $downloader;
    }

    public function testValidYoutubeCode()
    {
        $this->assertEquals('code', $this->downloader->youtubeCode('http://youtu.be/code'));
        $this->assertEquals('code', $this->downloader->youtubeCode('http://youtu.be/code?test'));
        $this->assertEquals('code', $this->downloader->youtubeCode('http://youtube.com/embed/code'));
        $this->assertEquals('code', $this->downloader->youtubeCode('https://youtube.com/embed/code'));
        $this->assertEquals('code', $this->downloader->youtubeCode('http://youtube.com/embed/code?test'));
        $this->assertEquals('code', $this->downloader->youtubeCode('https://youtube.com/embed/code?test'));
        $this->assertEquals('code', $this->downloader->youtubeCode('http://youtube.com/watch/?v=code'));
        $this->assertEquals('code', $this->downloader->youtubeCode('https://youtube.com/watch/?v=code'));
    }

    public function testInvalidYoutubeCode()
    {
        $this->assertEquals(null, $this->downloader->youtubeCode('http://not-valid-youtube'));
        $this->assertEquals(null, $this->downloader->youtubeCode('http://youtu.be/'));
        $this->assertEquals(null, $this->downloader->youtubeCode('http://youtu.be/?test'));
        $this->assertEquals(null, $this->downloader->youtubeCode('http://youtube.com/embed'));
        $this->assertEquals(null, $this->downloader->youtubeCode('http://www.youtube.com/embed/?test'));
        $this->assertEquals(null, $this->downloader->youtubeCode('http://youtube.com/watch/v=code'));
        $this->assertEquals(null, $this->downloader->youtubeCode('https://youtube.com/watch/?s=code'));
    }

    public function testDownload()
    {
        $this->mockParentDownloadSuccess();
        $this->downloader->shouldReceive('youtubeCode')->andReturn('code');

        $attachment = $this->downloader->download('https://youtube.com/watch/?v=code');

        $this->assertInstanceOf(Youtube::class, $attachment);
        $this->assertEquals('code', $attachment->getCode());
        $this->assertEquals('https://www.youtube.com/watch?v=code', $attachment->getRealUrl());
        $this->assertEquals('https://www.youtube.com/embed/code', $attachment->getEmbedUrl());
    }

    public function testDownloadWithNotMaxRes()
    {
        $response = $this->createResponseMock(200);
        $responseFail = $this->createResponseMock(404);
        $this->client->shouldReceive('get')->with('http://img.youtube.com/vi/code/maxresdefault.jpg', Mockery::any())->andReturn($responseFail);
        $this->client->shouldReceive('get')->with('http://img.youtube.com/vi/code/hqdefault.jpg', Mockery::any())->andReturn($response);
        $this->filesystem->shouldReceive('put')->once();

        $this->downloader->shouldReceive('youtubeCode')->andReturn('code');

        $attachment = $this->downloader->download('https://youtube.com/watch/?v=code');

        $this->assertInstanceOf(Youtube::class, $attachment);
    }

    /**
     * @expectedException \App\Supports\Attachment\ResourceNotFoundException
     */
    public function testDownloadWithNotFoundCode()
    {
        $this->mockParentDownloadAllFail();
        $this->downloader->shouldReceive('youtubeCode')->andReturn('code');

        $this->downloader->download('https://youtube.com/watch/?v=code');
    }

    protected function mockParentDownloadAllFail()
    {
        $responseFail = $this->createResponseMock(404);
        $this->client->shouldReceive('get')->andReturn($responseFail);

        $this->filesystem->shouldReceive('put')->never();
    }

    protected function mockParentDownloadSuccess()
    {
        $response = $this->createResponseMock(200);
        $this->client->shouldReceive('get')->andReturn($response);

        $this->filesystem->shouldReceive('put')->once();
    }

    protected function createResponseMock($status)
    {
        $response = Mockery::mock(Response::class);
        $response->shouldReceive('getStatusCode')->andReturn($status);
        $response->shouldReceive('getHeader')->with(RedirectMiddleware::HISTORY_HEADER)->andReturn(['http://effective/url']);
        $response->shouldReceive('getHeader')->with('Content-Type')->andReturn('mime');
        $response->shouldReceive('getHeader')->with('Content-Length')->andReturn(1000);
        $response->shouldReceive('getBody')->andReturn('data');

        return $response;
    }

    public function testValidate()
    {
        $this->assertTrue($this->downloader->validate('http://youtu.be/code'));
        $this->assertTrue($this->downloader->validate('http://youtu.be/code?test'));
        $this->assertTrue($this->downloader->validate('http://youtube.com/embed/code'));
        $this->assertTrue($this->downloader->validate('https://youtube.com/embed/code'));
        $this->assertTrue($this->downloader->validate('http://youtube.com/embed/code?test'));
        $this->assertTrue($this->downloader->validate('https://youtube.com/embed/code?test'));
        $this->assertTrue($this->downloader->validate('http://youtube.com/watch/?v=code'));
        $this->assertTrue($this->downloader->validate('https://youtube.com/watch/?v=code'));
    }

    public function testValidateNotValid()
    {
        $this->assertFalse($this->downloader->validate('http://not-valid-youtube'));
        $this->assertFalse($this->downloader->validate('http://youtu.be/'));
        $this->assertFalse($this->downloader->validate('http://youtu.be/?test'));
        $this->assertFalse($this->downloader->validate('http://youtube.com/embed'));
        $this->assertFalse($this->downloader->validate('http://www.youtube.com/embed/?test'));
        $this->assertFalse($this->downloader->validate('http://youtube.com/watch/v=code'));
        $this->assertFalse($this->downloader->validate('https://youtube.com/watch/?s=code'));
    }
}
